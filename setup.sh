#!/bin/bash

echo "[i] Installing Required Packages..."
apt-get install -y pandoc texlive


clear
clear
echo "[+] Packages installed!"

echo "[i] Configuring Files..."

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
VERSION=$($DIR/tools/getVersion.sh)

rm src/config.php
cp src/config.php.def src/config.php
sed -i "s/@@VERSION_VERSION@@/$VERSION/g" src/config.php

## Generating Documentations
rm -rf $DIR/doc/error/*.pdf
DIRECTORY=$DIR/doc/error
for entry in $DIR/doc/error/*
do
	newName="$entry.pdf"
	pandoc $entry -o $newName
done

echo "[i] Installing Backend to '/var/www/html/api/'"

if [ ! -d /var/www/html/api ]; then
	mkdir /var/www/html/api
else
	rm -rf /var/www/html/api/*
fi

cp -r $DIR/src/* /var/www/html/api

echo "[+] Installed Backend :)"

echo "[i] Setup MySQL Databases"
mysql -uroot -p < $DIR/setup/SQL/setup_database.sql


echo -e "     [i] Enter new MySQL Username [default: uibackend]: \c"
read mysqlUsername

if [ -d $mysqlUsername ]; then
	mysqlUsername="uibackend"
fi

mysqlPassword=$(cat /dev/urandom | tr -dc 'a-zA-Z0-9' | fold -w 16 | head -n 1)

echo "GRANT ALL PRIVILEGES ON intern__intern.* TO '$mysqlUsername'@'localhost' IDENTIFIED BY '$mysqlPassword';" > /tmp/SQL_setup.sql
echo "GRANT ALL PRIVILEGES ON intern__projects__prj.* TO '$mysqlUsername'@'localhost' IDENTIFIED BY '$mysqlPassword';" >> /tmp/SQL_setup.sql
echo "GRANT ALL PRIVILEGES ON intern__download_site.* TO '$mysqlUsername'@'localhost' IDENTIFIED BY '$mysqlPassword';" >> /tmp/SQL_setup.sql
echo "GRANT ALL PRIVILEGES ON intern__projects__version.* TO '$mysqlUsername'@'localhost' IDENTIFIED BY '$mysqlPassword';" >> /tmp/SQL_setup.sql
echo "GRANT ALL PRIVILEGES ON intern__system.* TO '$mysqlUsername'@'localhost' IDENTIFIED BY '$mysqlPassword';" >> /tmp/SQL_setup.sql
mysql -uroot -p < /tmp/SQL_setup.sql

echo "     [i] ATTENTION: MySql User Password: $mysqlPassword"

rm /tmp/*.sql

echo "[+] Created Databases"
