#!/bin/bash
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

echo -e "${YELLOW}"

# import Colors/ Settings
$DIR/__settings.sh

## START ##
echo -e "${DARK_GRAY}[${YELLOW}-${DARK_GRAY}]${NC} The old Configuration and Folders will be deleted!"

# get Ressources Location
echo -e "[i] Where are your Resource Files located? [default: res/]: \c"
read resourcesLocation

if [ -z "$resourcesLocation"]; then
	resourcesLocation="$DIR/res/"
fi

# get Binarys
binarys=( )
binarysNum=0

while true
do
	echo -e "[i] Add Binary Name (this File will be Searched [recursivly], starting from the Root Dir): "
	read binaryName

	if [ -z "$binaryName" ]; then
		break
	fi

	# increase 'binaryNum'
	binaryNum=$((binaryNum+1))

	binarys[$binaryNum]="$binaryName"
done

# get API-Server URL
echo -e "[i] API-Server URL: "
read apiServerURL

if [ -z "$apiServerURL" ]; then
	echo "[-] Please Enter a URL or IP to the API Server!!"
	exit -1
fi

##############################
##   Generating JSON File   ##
##############################
jsonFile=$(jo -p binarys=$binaryNum)

