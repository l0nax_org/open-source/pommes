#!/bin/bash

__JASDUEZASD_SETTINGS_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

## Colors ##
BLACK="\033[0;30m"
RED='\033[0;31m'
GREEN="\033[0;32m"
LIGHT_GRAY="\033[0;37m"
DARK_GRAY="\033[1;30m"
YELOW="\033[1;33m"

NC='\033[0m' # No Color

## Default Settings ##
updateDir="$__JASDUEZASD_SETTINGS_DIR/../../update"
