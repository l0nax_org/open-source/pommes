#!/bin/bash
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

echo "[i] Packages Required:"
echo "      - build-essential"
echo "      - make"
echo "      - tar"
echo "      - gzip, gunzip

## creating 'jo'
cd $DIR/../bin/

tar xf xvzf jo-1.0.tar.gz  || exit -1
cd jo-1.
./configure       || exit -1
make check        || exit -1
make install      || exit -1
