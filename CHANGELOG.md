# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/).<br>
Read more for the advanced Changelog Syntax in the Contributing Guide!<br>


# [1.1.0] - Unreleased
### Added
- Add scripts for API Handling via Command Line
  * createProject.sh
  * createPlan.sh
- Add 6 new API Calls
  * /create_project
  * /add_plan
  * /os/list_os
  * /os/get_version/{osID}
  * /os/get_arch
- Add default Documentation
- Add API Documentation
- Print Avaible Architectures on 'createPlan.sh' Script

### Fixed
- Fix API Call Errors
- Fix Access Token Error Print in project.php


### [1.1.0-0] - 2018-05-24
#### Added
- Add scripts for API Handling via Command Line
  * createProject.sh
  * createPlan.sh
- Add 6 new API Calls
  * /create_project
  * /add_plan
  * /os/list_os
  * /os/get_version/{osID}
  * /os/get_arch
- Add default Documentation
- Add API Documentation


# [1.0.0] - 2018-04-25 
### Added
- All Basic Project Files

