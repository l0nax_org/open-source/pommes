# Dashboard (backend)

## Project Informations

### Branches
**Master** &nbsp;&nbsp;[![pipeline status](https://gitlab.com/l0nax_org/intern/dashboard_backend/badges/master/pipeline.svg)](https://gitlab.com/l0nax_org/intern/dashboard_backend/commits/master) [![coverage report](https://gitlab.com/l0nax_org/intern/dashboard_backend/badges/master/coverage.svg)](https://gitlab.com/l0nax_org/intern/dashboard_backend/commits/master) <br/>
**Develop** [![pipeline status](https://gitlab.com/l0nax_org/intern/dashboard_backend/badges/develop/pipeline.svg)](https://gitlab.com/l0nax_org/intern/dashboard_backend/commits/develop) [![coverage report](https://gitlab.com/l0nax_org/intern/dashboard_backend/badges/develop/coverage.svg)](https://gitlab.com/l0nax_org/intern/dashboard_backend/commits/develop)

## Setup
 Run ```setup.sh``` it will install all required things.

## === Developing Documentation ===
### Database Informations
**OS Data:**<br/>
If a Project is for Linux, Windows and Mac OS X than add to 'os_id' (eg.) this: "0; 1; 2;".<br/>
And if the OS Version does not matter insert this to 'os_ver_id': "NULL".<br/>

**Version (intern__projects__version) add Project Example:**<br/>
- Tablename: 5af4b826ce0b1

### Files
- [configSetup.sh](ci/setup/configSetup.sh)
  Create the Config for the Automatisation Process (Update, automated Download Site, ..).
- [checkFiles.sh](ci/scripts/checkFiles.sh)
  Compare local files with files on the server, and generate updateFiles.json in 'update' Directory.<br>
  Don't run this Script manual!!

### Privileges/ Permissions
To see the Permissions please read the [Web Documentation](doc/permissions.html).<br>
**Warning: To read the Documentation you must "run" the Documentation and read them. Your can run `readWebDoc.sh` to read the Documentation.**

### Actions
**For all Actions is a Access Token Required!! This Token will not be written extra at the POST/ GET Sections of the Actions! JSON ID: 'access_token'**<br/>
**And the JSON String MUST be encoded with base64!! And send the Data (base64 encoded JSON String) only with GET!!**<br/>
**The JSON String has a Variable Name:<br/>**
**- GET:  data**<br/>

<!-- TODO: Übertragen dieser API Dokumentation in die API Dokumentation -->
##### (action) Check File
**Description:** Check if Files has Changed or not (via Checksum). It Exists a [Script](ci/scripts/checkFiles.sh) to automated Check the Files.<br/>
**URI: /update/check_files**<br/>
**POST Data:**<br/>
  - JSON String:
    * Unique Project ID            || ppid
    * File Name                    || file_name
    * File Path (from Git Root)    || file_path
    * File Hash (SHA512)           || sha512

**Return:**<br/>
  - (File Exists) Returns SHA512 Hash of File
  - (File does not Exists) Returns: "08558"
