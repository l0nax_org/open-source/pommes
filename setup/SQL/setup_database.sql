-- 
-- phpMyAdmin SQL Dump
-- version 4.6.6deb4
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: May 20, 2018 at 12:24 AM
-- Server version: 10.1.26-MariaDB-0+deb9u1
-- PHP Version: 7.0.27-0+deb9u1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `intern__download_site`
--
CREATE DATABASE IF NOT EXISTS `intern__download_site` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;
USE `intern__download_site`;

-- --------------------------------------------------------

--
-- Table structure for table `templates`
--

CREATE TABLE `templates` (
  `df_tid` int(11) NOT NULL COMMENT 'Download Site Template ID',
  `title` text NOT NULL COMMENT 'Template Title',
  `comment` text NOT NULL COMMENT 'Template Comment',
  `author` mediumtext NOT NULL COMMENT 'Template Author/ Creator'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='Project Download Templates';

--
-- Indexes for dumped tables
--

--
-- Indexes for table `templates`
--
ALTER TABLE `templates`
  ADD PRIMARY KEY (`df_tid`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `templates`
--
ALTER TABLE `templates`
  MODIFY `df_tid` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Download Site Template ID';--
-- Database: `intern__intern`
--
CREATE DATABASE IF NOT EXISTS `intern__intern` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;
USE `intern__intern`;

-- --------------------------------------------------------

--
-- Table structure for table `tokens`
--

CREATE TABLE `tokens` (
  `token_id` int(11) NOT NULL,
  `token` text NOT NULL,
  `privileges` mediumtext NOT NULL,
  `user_name` text NOT NULL,
  `comment` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='All the Access Tokens';

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tokens`
--
ALTER TABLE `tokens`
  ADD PRIMARY KEY (`token_id`),
  ADD UNIQUE KEY `tokens_token_id_uindex` (`token_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tokens`
--
ALTER TABLE `tokens`
  MODIFY `token_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;--
-- Database: `intern__projects__prjs`
--
CREATE DATABASE IF NOT EXISTS `intern__projects__prjs` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;
USE `intern__projects__prjs`;

-- --------------------------------------------------------

--
-- Table structure for table `ids`
--

CREATE TABLE `ids` (
  `ppid` text NOT NULL COMMENT 'Uniqe Project (+Plan) ID',
  `prj_id` int(11) NOT NULL,
  `pl_id` int(11) NOT NULL,
  `os_id` text NOT NULL COMMENT 'OS Name (Seperated by '';'')',
  `os_ver_id` text COMMENT 'OS Version (Seperated by '';'')',
  `os_arch_id` text COMMENT 'OS Architekture (Seperated by '';'')'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='All IDs of the Project';

-- --------------------------------------------------------

--
-- Table structure for table `info`
--

CREATE TABLE `info` (
  `p_iid` int(11) NOT NULL COMMENT '(Project) Info ID',
  `prj_id` int(11) NOT NULL COMMENT 'Project ID',
  `download_enabled` tinyint(1) NOT NULL COMMENT 'Automated Download Page activated?',
  `download_url` text NOT NULL COMMENT 'Download URL of the Project'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='Special Informations about Projects';

-- --------------------------------------------------------

--
-- Table structure for table `plans`
--

CREATE TABLE `plans` (
  `pl_id` int(11) NOT NULL COMMENT 'Plan ID',
  `prj_id` int(11) NOT NULL,
  `plan_name` text NOT NULL,
  `access_token` text NOT NULL COMMENT 'Special Access Token (for create Updates, etc.)'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='Software Plans (eg.: Free, Premium)';

-- --------------------------------------------------------

--
-- Table structure for table `prjs`
--

CREATE TABLE `prjs` (
  `prj_id` int(11) NOT NULL COMMENT 'Project ID',
  `project_name` text NOT NULL,
  `comment` mediumtext,
  `download_page` tinyint(1) NOT NULL COMMENT 'Automated Download Page activated?'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `info`
--
ALTER TABLE `info`
  ADD PRIMARY KEY (`p_iid`);

--
-- Indexes for table `plans`
--
ALTER TABLE `plans`
  ADD PRIMARY KEY (`pl_id`);

--
-- Indexes for table `prjs`
--
ALTER TABLE `prjs`
  ADD PRIMARY KEY (`prj_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `info`
--
ALTER TABLE `info`
  MODIFY `p_iid` int(11) NOT NULL AUTO_INCREMENT COMMENT '(Project) Info ID';
--
-- AUTO_INCREMENT for table `plans`
--
ALTER TABLE `plans`
  MODIFY `pl_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Plan ID';
--
-- AUTO_INCREMENT for table `prjs`
--
ALTER TABLE `prjs`
  MODIFY `prj_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Project ID';--
-- Database: `intern__projects__version`
--
CREATE DATABASE IF NOT EXISTS `intern__projects__version` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;
USE `intern__projects__version`;

-- --------------------------------------------------------

--
-- Table structure for table `list`
--

CREATE TABLE `list` (
  `v_lid` int(11) NOT NULL COMMENT 'Version List ID',
  `ppid` text NOT NULL COMMENT 'Uniqe Project (+Plan) ID',
  `latest_version` text NOT NULL COMMENT 'Latest Version of the Project'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='List of all Version Informations';

--
-- Indexes for dumped tables
--

--
-- Indexes for table `list`
--
ALTER TABLE `list`
  ADD PRIMARY KEY (`v_lid`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `list`
--
ALTER TABLE `list`
  MODIFY `v_lid` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Version List ID';--
-- Database: `intern__system`
--
CREATE DATABASE IF NOT EXISTS `intern__system` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;
USE `intern__system`;

-- --------------------------------------------------------

--
-- Table structure for table `architecture`
--

CREATE TABLE `architecture` (
  `os_aid` int(11) NOT NULL COMMENT 'Architecture ID',
  `architecture` text NOT NULL COMMENT 'eg.: x86_64'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `os`
--

CREATE TABLE `os` (
  `os_id` int(11) NOT NULL,
  `os_name` int(11) NOT NULL COMMENT 'eg.: Windows'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='Operating Systems';

-- --------------------------------------------------------

--
-- Table structure for table `os_version`
--

CREATE TABLE `os_version` (
  `os_vid` int(11) NOT NULL COMMENT 'OS Version ID',
  `os_id` int(11) NOT NULL,
  `Version` text NOT NULL COMMENT 'eg.: 7'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='OS Versions ';

--
-- Indexes for dumped tables
--

--
-- Indexes for table `architecture`
--
ALTER TABLE `architecture`
  ADD PRIMARY KEY (`os_aid`);

--
-- Indexes for table `os`
--
ALTER TABLE `os`
  ADD PRIMARY KEY (`os_id`);

--
-- Indexes for table `os_version`
--
ALTER TABLE `os_version`
  ADD PRIMARY KEY (`os_vid`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `architecture`
--
ALTER TABLE `architecture`
  MODIFY `os_aid` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Architecture ID';
--
-- AUTO_INCREMENT for table `os`
--
ALTER TABLE `os`
  MODIFY `os_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `os_version`
--
ALTER TABLE `os_version`
  MODIFY `os_vid` int(11) NOT NULL AUTO_INCREMENT COMMENT 'OS Version ID';
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
