#!/bin/bash
#===============================================================================
#
#          FILE:  createPlan.sh
#
#         USAGE:  ./createPlan.sh
#
#   DESCRIPTION:  Create/ Add a Plan to a Project.
#
#       OPTIONS:  ---
#  REQUIREMENTS:  curl base64 jq
#          BUGS:  ---
#         NOTES:  ---
#        AUTHOR:  Francesco Emanuel Bennici (), benniciemanuel78@gmail.com
#       COMPANY:  l0nax
#       VERSION:  1.0
#       CREATED:  22.05.2018 01:17:27 CEST
#      REVISION:  ---
#===============================================================================

file="/tmp/createPlan_8QTafl1D2AKYz5DAcFHln8aEZvsxploDxZiIGDmn5alwjXST1g0UlaTW.txt"

echo "*** INFORMATION: If this is your first Run of these Scripts please run './setup.sh' first!! ***"
echo
. ./.settings

## === START ===
echo -e "Please enter the Project ID: \c"
read prjID

echo -e "Please enter the Plan Name [eg. \"Community Edition\"]: \c"
read planName

echo "***** OS Informations *****"
echo "Avaible OS's:"
./_scripts/getOS.sh

echo "***************************"

echo -e "Please enter the System ID(s) for the Plan [eg. 1, 2]: \c"
read osID

## get OS ID and Print avaible Version IDs
IFS=',' read -ra OSID <<< "$osID"
for i in "${OSID[@]}"; do
	echo "OS ID #${i}:"
	./_scripts/getOSVersion.sh $i
	echo "----------------"
done

echo -e "Please enter the System Version ID(s) for the Plan [eg. 1, 2]: \c"
read osVID

echo "***** Avaible Architectures *****"
./_scripts/getOSArch.sh
echo "*********************************"

echo -e "Please enter the Architecture ID(s) for the Plan [eg. 1, 2]: \c"
read arcID

## creating File
echo -e "{\"prj_id\":\"${prjID}\",\"access_token\":\"${TOKEN}\",\"plan_name\":\"${planName}\",\"os_id\":\"${osID}\",\c" > $file
echo -e "\"os_vid\":\"${osVID}\",\"os_aid\":\"${arcID}\"}\c" >> $file

## encode File
data=$(cat $file | tr -d '\n')
ret=$(curl "${API_URL}add_plan" -d "${data}" -H "Content-Type: application/json" -X POST --silent)

if [[ ${ret:0:5} == "Error" ]]; then
    echo "[-] There is a Error:"
    echo $ret
    echo
    exit -1
fi

## no errors => print Basic Data
ppid=$(echo $ret | jq -r '.ppid')
token=$(echo $ret | jq -r '.new_token')

echo
echo "[+] Created Plan Successfull"
echo "***** Project Informations *****"
echo "Plan Name                 : $planName"
echo "Special/ New Access Token : $token"
echo "Project ID                : $prjID"
echo "OS ID(s)                  : $osID"
echo "OS Version ID(s)          : $osVID"
echo "Architecture ID(s)        : $arcID"
echo "Unique Project ID (ppid)  : $ppid"
echo "********************************"

## cleaning Up
#rm $file
