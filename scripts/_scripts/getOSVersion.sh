#!/bin/bash
#===============================================================================
#
#          FILE:  getOSVersion.sh
#
#         USAGE:  ./getOSVersion.sh <OS ID>
#		OS ID: <int> Only ONE ID!!
#
#   DESCRIPTION:  Print the Avaible Versions from the OS.
#
#       OPTIONS:  ---
#  REQUIREMENTS:  curl
#          BUGS:  ---
#         NOTES:  ---
#        AUTHOR:  Francesco Emanuel Bennici (), benniciemanuel78@gmail.com
#       COMPANY:  l0nax
#       VERSION:  1.0
#       CREATED:  24.05.2018 15:53:03 CEST
#      REVISION:  ---
#===============================================================================

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

file="/tmp/getOSVersion_rpv3CqMHf2aabsQ456Gidxt7mBqk47ZHO4aNT3kZIl9o7593tK"

. ${DIR}/../.settings

## === START ===


## get Versions
osID="$1"
curl "${API_URL}os/get_version/${osID}" --silent > $file

## print Data in Table
./_scripts/printTable.sh ',' "$(cat $file)"

## clean up
rm -f ${file}*

## =============
