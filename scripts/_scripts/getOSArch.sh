#!/bin/bash
#===============================================================================
#
#          FILE:  getOSArch.sh
# 
#         USAGE:  ./getOSArch.sh 
# 
#   DESCRIPTION:  Print a Table of all avaible Architectures
# 
#       OPTIONS:  ---
#  REQUIREMENTS:  curl
#          BUGS:  ---
#         NOTES:  ---
#        AUTHOR:  Francesco Emanuel Bennici (), benniciemanuel78@gmail.com
#       COMPANY:  l0nax
#       VERSION:  1.0
#       CREATED:  24.05.2018 17:40:02 CEST
#      REVISION:  ---
#===============================================================================


DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

file="/tmp/getOSArch_dXl1WQLxu196nzk0cetwHqZAalfZ8HxIsCSBGnzQQRX7jNmCoU"

. ${DIR}/../.settings

## === START ===

## get Architectures
curl "${API_URL}os/get_arch" --silent > $file

## print Table
./_scripts/printTable.sh ',' "$(cat $file)"

## cleaning up
rm -f ${file}*
