#!/bin/bash
#===============================================================================
#
#          FILE:  getOS.sh
#
#         USAGE:  ./getOS.sh
#
#   DESCRIPTION:  Get all avaible OS from the Server and print them in a Table
#
#       OPTIONS:  ---
#  REQUIREMENTS:  curl jq base64
#          BUGS:  ---
#         NOTES:  ---
#        AUTHOR:  Francesco Emanuel Bennici (), benniciemanuel78@gmail.com
#       COMPANY:  l0nax
#       VERSION:  1.0
#       CREATED:  23.05.2018 00:00:05 CEST
#      REVISION:  ---
#===============================================================================

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

file="/tmp/getOS_Ke2IK0XsQofJDRr3rCkABvaADzJ1yjocGKTWntaPfcGT3VEHhv"

. ${DIR}/../.settings

### === START ===

## get OS
curl --silent "${API_URL}os/list_os" > $file

## generate Table
./_scripts/printTable.sh ',' "$(cat $file)"

## cleaning up
rm -f $file
