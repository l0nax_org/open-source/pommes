#!/bin/bash
#===============================================================================
#
#          FILE:  createProject.sh
#
#         USAGE:  ./createProject.sh
#
#   DESCRIPTION:  Script to Create a new Project (interacting with the 'database_backend' via cURL)
#
#       OPTIONS:  ---
#  REQUIREMENTS:  curl base64 jq
#          BUGS:  ---
#         NOTES:  ---
#        AUTHOR:  Francesco Emanuel Bennici (), benniciemanuel78@gmail.com
#       COMPANY:  l0nax
#       VERSION:  1.0
#       CREATED:  21.05.2018 20:53:53 CEST
#      REVISION:  ---
#===============================================================================

file="/tmp/createNewProject_j73hasd73hsef73ksdf83hsdf8u83asefdj8s3jsdfusdfhz3sdfhz3.txt"

echo "*** INFORMATION: If this is your first Run of these Scripts please run './setup.sh' first!! ***"
echo
. ./.settings

## === START ===
echo -e "Enter Project Name: \c"
read prjName

if [ -z "$prjName" ]; then
	echo "[-] Error: Please Enter a (valid) Project Name!"
	exit -1
fi

echo -e "Enter Project Comment: \c"
read prjComment

echo -e "Enable automated Download Site? (yn) [y]: \c"
read enableDown

if [ -z "$enableDown" ]; then
	enableDown="y"
fi

## creating Files
echo -e "{\"prj_name\":\"${prjName}\", \"prj_comment\":\"${prjComment}\", \"access_token\":\"${TOKEN}\", \"download_site\":\c" > $file

if [ $enableDown = "y" ]; then
	echo 'true}' >> $file
else
	echo 'false}' >> $file
fi

## make Request
data=$(cat $file | tr -d '\n')
server_ret=$(curl "${API_URL}create_project" -d "${data}" -H "Content-Type: application/json" -X POST --silent)

# check if Return starts with 'Error '
if [[ ${server_ret:0:5} == "Error" ]]; then
    echo "[-] There is a Error:"
    echo $server_ret
    echo
    exit -1
fi

## no errors => Print Basic Data
prjID=$(echo $server_ret | jq -r '.prj_id')

echo
echo "[+] Created Project Successfull"
echo "***** Project Informations *****"
echo "Project Name   : $prjName"
echo "Project Comment: $prjComment"
echo "Project ID     : $prjID"
echo "********************************"

## cleaning Up
rm $file
