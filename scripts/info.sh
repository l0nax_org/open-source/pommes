#!/bin/bash
#===============================================================================
#
#          FILE:  info.sh
#
#         USAGE:  ./info.sh
#
#   DESCRIPTION:  Call the Information String from the Backend.
#
#       OPTIONS:  ---
#  REQUIREMENTS:  curl jq
#          BUGS:  ---
#         NOTES:  ---
#        AUTHOR:  Francesco Emanuel Bennici (), benniciemanuel78@gmail.com
#       COMPANY:  l0nax
#       VERSION:  1.0
#       CREATED:  22.05.2018 02:01:52 CEST
#      REVISION:  ---
#===============================================================================

. ./.settings

## === START ===

ret=$(curl "${API_URL}info" --silent)

version=$(echo $ret | jq -r '.version')

## print Informations
echo "***** Informations *****"
echo "Backend/ API Version               : $version"
echo "************************"

