#!/bin/bash
#===============================================================================
#
#          FILE:  setup.sh
#
#         USAGE:  ./setup.sh
#
#   DESCRIPTION:  Create the default Files for the User.
#
#       OPTIONS:  ---
#  REQUIREMENTS:  ---
#          BUGS:  ---
#         NOTES:  ---
#        AUTHOR:  Francesco Emanuel Bennici (), benniciemanuel78@gmail.com
#       COMPANY:  l0nax
#       VERSION:  1.0
#       CREATED:  21.05.2018 21:16:16 CEST
#      REVISION:  ---
#===============================================================================

file=".settings"

## delete Settings
rm -f $file

## get Input
echo -e "Enter your API URL (please append '/' at the end): \c"
read apiURL

echo -e "Enter your Access Token: \c"
read accessToken

## write Data to File
echo "export API_URL=\"${apiURL}\"" >> $file
echo "export TOKEN=\"${accessToken}\"" >> $file


### Print User Information
echo "Please install the following Tools:"
echo "- curl"
echo "- base64"
echo "- jq"