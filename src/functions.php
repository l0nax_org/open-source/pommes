<?php
/**
 * Created by PhpStorm.
 * User: Emanuel Bennici <benniciemanuel78@gmail.com>
 * Date: 15.05.18
 * Time: 08:33
 */

/**
 * Connect to the Database
 * @return mysqli
 */
function connectDB() {
	$config = include "config.php";

	$db_host = $config['db']['host'];
	$db_user = $config['db']['user'];
	$db_pass = $config['db']['password'];

	$db = mysqli_connect( $db_host, $db_user, $db_pass );

	if ( $db->connect_error ) {
		die( "Connection failed: " . mysqli_connect_error() );
	}

	return $db;
}

/**
 * Check if String Starts with ..
 *
 * @param $startsWith       String to search.
 * @param $string           String to Check
 *
 * @return bool
 */
function startsWith( $startsWith, $string ) {
	$len_string = strlen( $string );
	$len_stW    = strlen( $startsWith );

	if ( $len_string < $len_stW ) {
		return false;
	}

	for ( $i = 0; $i < $len_stW; $i ++ ) {
		if ( $string[ $i ] !== $startsWith[ $i ] ) {
			return false;
		}
	}

	return true;
}


/**
 * Prevent a SQL Injection
 *
 * @param $sql_input
 *
 * @return array|string
 */
function preventSQLI( $sql_input ) {
	$str = $sql_input;

	$statements[] = array(
		'SELECT',
		'DROP',
		'CREATE',
		'INSERT',
		'UPDATE',
		'FROM',
		'WHERE',
		'AND',
		'ORDER',
		'BY',
		'DESC',
		'INDEX',
		'INTO',
		'UNION',
		'JOIN',
		'EXPLAIN',
		'USE',
		'SHOW',
		'TABLE',
		'DESCRIBE',
		'SET',
		'ALTER',
		'DELETE',
		'FLUSH',
		';',
		'EXIT',
		'QUIT',
		'GRANT',
		'PRIVILEGES',
		'DATABASE',
		'LOAD',
		'INFILE',
		'BACKUP',
		'INDEX',
		'CHECK',
		'COMMIT',
		'MASTER',
		'VIEW',
		'DO',
		'USER',
		'KILL',
		'OPTIMIZE',
		'RESTORE',
		'REVOKE'
	);

	$num = count( $statements );

	for ( $i = 0; $i < $num; $i ++ ) {
//		$str = preg_grep( "/" . $statements[ $i ] . "/i", "/1e9debbf08/", $str );
		$str = str_ireplace( $statements[ $i ], "1e9debbf08", $str );
	}

	// default MySQL SQL Injection-Prevention
//	$str = stripslashes( $str );

//    $str = mysql_real_escape_string($str);

	return $str;
}

/**
 * Check if the Token has the Privilege to do ...
 *
 * @param $token string     The Access Token
 * @param $priv  string     Privilege String
 *
 * @return int              It Returns '1' if the Token is valid and has the Privilege for that Action, Returns '-1' if the token has not
 *                          the Privileges for that Action, Returns '0' if the Token does not exists.
 */
function checkToken( $token, $priv ) {
	// connect to Database
	$db = connectDB();

	$userToken = preventSQLI( $token );
	$perm      = preventSQLI( $priv );

	// get the Privileges of the Token
	$sql    = "SELECT privileges FROM intern__intern.tokens WHERE token='" . $userToken . "';";
	$result = $db->query( $sql );

	if ( $result->num_rows > 0 ) {
		$row = $result->fetch_assoc();

		if ( strpos( $row['privileges'], $perm ) !== false ) {
			return 1;
		} elseif ( strpos( $row['privileges'], "admin" ) !== false ) {
			return 1;
		} else {
			return -1;
		}
	} else {
		return 0;
	}
}

/**
 * Generate a Random String
 *
 * @param int $length       The length of the Random String
 *
 * @return string           The Random generated String
 */
function generateRandomString($length = 10) {
	$characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
	$charactersLength = strlen($characters);
	$randomString = '';
	for ($i = 0; $i < $length; $i++) {
		$randomString .= $characters[rand(0, $charactersLength - 1)];
	}
	return $randomString;
}