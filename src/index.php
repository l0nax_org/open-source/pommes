<?php

require_once 'config.php';
require_once 'project.php'; // for 'create_project' and 'create_plan'
require_once 'functions.php';
require_once 'operating_system.php';

$uri = parse_url( $_SERVER['REQUEST_URI'] )['path'];

/// TODO: Switch $uri to 'parse_url($_SERVER['REQUEST_URI'])['path']'!! Example Output: "/api/test"
//
//$method = $_SERVER['REQUEST_METHOD'];
//$request = explode('/', trim($_SERVER['PATH_INFO'],'/'));
//$input = json_decode(file_get_contents('php://input'),true);
//
//echo "*** INFORMATIONS ***\n";
//echo "method: $method\n";
//echo "request:";
//print_r($request);
//echo "\n----\n";
//echo "input: $input\n";
//echo "PATH_INFO: ${_SERVER['PATH_INFO']}";
//print_r($_SERVER['PATH_INFO']);
//echo "\n";
//
//print_r($_SERVER);
//
//echo "";

// Debug/ Informations
if ( $uri == "/info" ) {
	$config = include "config.php";
	$str    = json_encode( array(
		"version" => $config['version']
	) );

	echo $str;

	return;
}

//<editor-fold desc="Project">
if ( $uri == "/create_project" ) {
	// get Data
	$obj = json_decode( file_get_contents('php://input') );

	// get Token
	$token = $obj->{'access_token'};

	// get Project Name
	$prj_name = $obj->{'prj_name'};

	// get Project Comment
	$prj_comment = $obj->{'prj_comment'};

	// get Download Site Settings
	$download_site = $obj->{'download_site'};

	/// create Project
	$ret = create_project( $prj_name, $prj_comment, $download_site, $token );

	// check Return & return Data
	echo $ret;

	return;
} elseif ( $uri == "/add_plan" ) {
	// get Data
	$obj = json_decode( file_get_contents('php://input') );

	// get Token
	$token = $obj->{'access_token'};

	// get Project ID
	$prjID = $obj->{'prj_id'};

	// get Plan Name
	$planName = $obj->{'plan_name'};

	// get OS ID(s)
	$osID = $obj->{'os_id'};

	// get OS Version ID(s)
	$osVerID = $obj->{'os_vid'};

	// get Architecture ID
	$archID = $obj->{'os_aid'};

	$ret = create_plan( $prjID, $planName, $token, $osID, $osVerID, $archID );

	echo $ret;

	return;
}
//</editor-fold>

//<editor-fold desc="OS">
if ( $uri == "/os/list_os" ) {
	getOS();

	return;
} elseif ( dirname($uri) == "/os/get_version" ) {
	// get OS ID
	$osID = explode( "/", $uri )[3];
	getVersion( $osID );

	return;
} elseif ( $uri == "/os/get_arch" ) {
	getArch();

	return;
}
//</editor-fold>


// unknow Action
die( "Error ERR_UNKNOW_ACTION" );
