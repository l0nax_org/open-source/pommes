<?php
/**
 * Created by PhpStorm.
 * User: Emanuel Bennici <benniciemanuel78@gmail.com>
 * Date: 15.05.18
 * Time: 09:03
 */
return array(
	"0x001FFE" => "Error 0x001FFE: Unknow API Call",
	"0x002CFE" => "Error 0x002CFE: Unknow/ wrong Authentication Token",
	"0x002FGE" => "Error 0x002FGE: Wrong/ Unknow Parameters.",
	"0x002HGG" => "Error 0x002HGG: A Project with this Name already Exists!",

	/// ===== Access Token =====
	"0x003UAT" => "Error 0x003UAT: Unknow Access Token",
	"0x003AIR" => "Error 0x003AIR: Access Token has invalid Permissions!",

	"0x003ATE" => "Error 0x003ATE: There is a Error with your Access Token (0x003UAT, 0x003AIR, ...)",

	/// ===== Project/ Plan =====
	"0x004PAE" => "Error 0x004PAE: A Plan with this Name already exists for that Project with the ID {prj_id}.",

	/// ===== Other =====
	"0x005UEE" => "Error 0x005UEE: (Critical) Unknow Error.",
);