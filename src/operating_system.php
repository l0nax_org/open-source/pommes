<?php
/**
 * Created by PhpStorm.
 * User: Emanuel Bennici <benniciemanuel78@gmail.com>
 * Date: 24.05.18
 * Time: 15:25
 */
require_once "functions.php";


/**
 * Returns a "Table" of all avaible Operating Systems in the MySql Database.
 *
 * @return string       Table (like HEADER1,HEADER2,HEADER3...)
 */
function getOS() {
	$error = include "errors.php";
	$db    = connectDB();

	// get OS
	$sql    = "SELECT * FROM intern__system.os;";
	$result = $db->query( $sql );

	if ( $result->num_rows > 0 ) {
		echo "OS ID,System Name\n";

		while ( $row = $result->fetch_assoc() ) {
			echo $row['os_id'] . "," . $row['os_name'] . "\n";
		}
	} else {
		return $error['0x005UEE'];
	}
}

/**
 * Print the avaible OS Version.
 *
 * @param $os_id string|int
 */
function getVersion( $os_id ) {
	$error = include "errors.php";
	$db    = connectDB();

	// prevent SQL Injection
	$osID = preventSQLI( $os_id );

	// get OS
	$sql    = "SELECT os_vid, Version FROM intern__system.os_version WHERE os_id=${osID};";
	$result = $db->query( $sql );

	if ( $result->num_rows > 0 ) {
		echo "OS Version ID,Version\n";

		while ( $row = $result->fetch_assoc() ) {
			echo $row['os_vid'] . "," . $row['Version'] . "\n";
		}
	} else {
		echo $error['0x005UEE'];

		return;
	}
}

/**
 * Print all the Avaible Architectures
 */
function getArch() {
	$error = include "errors.php";
	$db    = connectDB();

	// get Arch's
	$sql    = "SELECT * FROM intern__system.architecture;";
	$result = $db->query( $sql );

	if ( $result->num_rows > 0 ) {
		echo "Arch ID,Architecture\n";

		while ( $row = $result->fetch_assoc() ) {
			echo $row['os_aid'].",".$row['architecture']."\n";
		}
	} else {
		echo $error['0x005UEE'];
		return;
	}
}