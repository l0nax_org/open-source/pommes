<?php
require 'config.php';

/**
 * Create a new Project
 *
 * @param $prjName          string  The Project Name
 * @param $prjComment       string  Project Comment
 * @param $download_enable  bool    Activate automated Download Site?
 * @param $token            string  The Access Token
 *
 * @return                  string  Return JSON Project String if Success, and a Error Code if Failed.
 */
function create_project( $prjName, $prjComment, $download_enable, $token ) {
	$error = include "errors.php";
	$db    = connectDB();

	// run SQL Injection Prevention
	$prjName    = preventSQLI( $prjName );
	$prjComment = preventSQLI( $prjComment );
	$userToken  = preventSQLI( $token );

	// check the Access Token
	$retToken = checkToken( $userToken, "project.create" );
	if ( $retToken === - 1 ) {
		return $error['0x003AIR'];
	} else if ( $retToken === 0 ) {
		return $error['0x003UAT'];
	}

	// check if Param 'download_enable' is a Boolean or not
	if ( ! is_bool( $download_enable ) ) {
		return $error['0x002FGE'] . "\nInformations: \n- download_enable: ${download_enable}";
	}

	$sql = "INSERT INTO intern__projects__prjs.prjs(prj_id, project_name, comment, download_page)" .
	       " VALUES(NULL, '" . $prjName . "', '" . $prjComment . "', '" . $download_enable . "');";

	$result = $db->query( $sql );

	if ( $result ) {
		// get Project ID
		$sql    = "SELECT prj_id FROM intern__projects__prjs.prjs WHERE project_name='" . $prjName . "';";
		$result = $db->query( $sql );

		if ( $result->num_rows > 0 ) {
			$row   = $result->fetch_assoc();
			$prjID = $row["prj_id"];

			// encode Return String
			return json_encode( array(
				'prj_name'      => $prjName,
				'prj_comment'   => $prjComment,
				'download_site' => $download_enable,
				'prj_id'        => $prjID
			) );
		} else {
			return $error['0x005UEE'];
		}

	} elseif ( ! $result ) {
		return $error['0x002HGG'] . "\n" . mysqli_error( $db );
	} else {
		return $error['0x005UEE'];
	}
}

/**
 * Create/ Add a Plan to a Project.
 *
 * @param $prj_id     string    The ID of the Project where to add the Plan.
 * @param $plan_name  string    The Name of the Plan
 * @param $token      string    The Access Token
 * @param $os_ID       string    The IDs of the OS's separated by ';'
 * @param $os_VerID    string    The IDs of the Version of the OS's separated by ';' (for all Versions: 'NULL')
 * @param $arch_ID     string    The IDs of the Architecture of the OS's separated by ';' (for all Versions: 'NULL')
 *
 * @return string               Return JSON Project String if Success, and a Error Code if Failed.
 */
function create_plan( $prj_id, $plan_name, $token, $os_ID, $os_VerID, $arch_ID ) {
	$error = include "errors.php";

	// connect DB
	$db = connectDB();

	// prevent SQL Injection
	$prjID       = preventSQLI( $prj_id );
	$planName    = preventSQLI( $plan_name );
	$accessToken = preventSQLI( $token );
	$osID        = preventSQLI( $os_ID );
	$osVerID     = preventSQLI( $os_VerID );
	$archID      = preventSQLI( $arch_ID );


	// check Token
	$ret = checkToken( $accessToken, "project." . $prjID . ".plan.add" );
	if ( $ret == - 1 || $ret == - 1 ) {
		return $error['0x003AIR'];
	} else if ( $ret == 0 ) {
		return $error['0x003UAT'];
	}

	// generate new Access Token for the Plan
	$newAccessToken = generateRandomString( 64 ); // 512 Bit

	// add Plan
	$sql    = "INSERT INTO intern__projects__prjs.plans(pl_id, prj_id, plan_name, access_token)
			VALUES (NULL, ${prjID}, '${planName}', '${newAccessToken}');";
	$result = $db->query( $sql );

	if ( $result ) {
		// generate PPID
		$ppid = generateRandomString( 5 );

		// get Plan ID
		$sql    = "SELECT pl_id FROM intern__projects__prjs.plans WHERE prj_id=${prjID} AND plan_name='${planName}';";
		$result = $db->query( $sql );

		if ( $result->num_rows > 0 ) {
			$row    = $result->fetch_assoc();
			$planID = $row['pl_id'];

			// insert Informations into 'ids' Table
			$sql    = "INSERT INTO intern__projects__prjs.ids(ppid, prj_id, pl_id, os_id, os_ver_id, os_arch_id) 
				VALUES ('${ppid}', ${prjID}, ${planID}, ${osID}, ${osVerID}, ${archID});";
			$result = $db->query( $sql );

			if ( $result ) {
				return json_encode( array(
					"ppid"   => $ppid,
					"prj_id" => $prjID,
					"pl_id"  => $planID,
//					"new_token" => $newAccessToken
				) );
			} else {
				return $error['0x005UEE'] . "\nMySQL Error: " . mysqli_error( $db ) . $sql;
			}
		} else {
			return $error['0x005UEE'];
		}
	} elseif ( ! $result ) {
		return str_replace( "{prj_id}", $prjID, $error['0x004PAE'] ) . "\nMySQL Error: " . mysqli_error( $db );
	} else {
		return $error['0x005UEE'];
	}
}